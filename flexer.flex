%%

%class Flexer
%unicode
/*%debug*/
%int
%line
%column

%{

    Context context;

	void addToV(char letter) {
		Context.allLetters.addSymbol(letter, true);
	}
	
	void addToE(char letter) {
		Context.containingLetters.addSymbol(letter, false);
	}
	
	void addToRKey(char key) {
		Context.tuples.addKey(key);
	}
	
	void addToRValue(char value) {
		Context.tuples.addValue(new Item(value, false));
	}
	
	String hasE() {
		return Context.hasE() ? "Yes" : "No";
	}
	
	String isVoid() {
		return Context.isVoid() ? "Yes" : "No";
	}
	
	void uselessChars() {
		Context.printUselessChars();
	}
	
	void checkSemError() {
		Context.checkSemError();
	}
	
	void checkSynError() throws java.io.IOException {
		Context.checkSynError();
	}
	
	void throwDefaultException() throws java.io.IOException {
		throw new java.io.IOException("Wrong character");
	}
%}

LineTerminator = \r|\n|\r\n
WS = {LineTerminator} | [ \t\f]
special = "\"" | "\$" | "+" | "%" | "&" | "#" | "." | "`" | "'" | "-" | "=" | "[" | "]" | ";" | "~" | "!" | "@" | "\^" | "*" | "+" | ":" | "|" | "<" | ">" | "?" | "_" | "\/" | "\\"
lower = [a-z]

Symbol = [:uppercase:] | {lower} | [:digit:] | {special}

%state CHARA TUPLE_VALUE SEPA
/* States:
   **SEPA: reads the separator inside the alphabet
   **CHARA: reads a symbol(character) of the alphabet
   **SEP: reads the separator between the alphabet and the expression
   **EXP: an operator is needed here because it's either in the beginning of
   ****the file, or after a binary operator
   **EXPO: the state after reading an operand. If another one follows, they
   ****are treated like the operands of a concatenation.
*/

%%

{WS}	{/*Skip whitespace in any state*/}
<YYINITIAL>	{//Read the alphabet
	"(" 	{
		    	yybegin(SEPA);
		    	Context.prevChar = '(';
			}
	[^(] 	{
				throwDefaultException();	
			}
}


<CHARA>	{//Read one symbol at a time
	{Symbol} {
			    String crtChar = yytext();
			    String symbol = yytext();
			    
			    if (Context.type == Context.Types.V) {
					yybegin(SEPA);
			    	addToV(symbol.charAt(0));
			    } else if (Context.type == Context.Types.E) {
					yybegin(SEPA);
			    	addToE(symbol.charAt(0));
			    } else if (Context.type == Context.Types.R) {
			    	if (Context.tuples.prevKey == null) {
				    	if (Context.prevChar != '(') {
							throwDefaultException();
						}
						
			    		yybegin(SEPA);
						addToRKey(symbol.charAt(0));
			    	} else {
			    		yybegin(TUPLE_VALUE);
			    		addToRValue(symbol.charAt(0));
			    	}
			    } else if (Context.type == Context.Types.S) {
			    	yybegin(SEPA);
			    	Context.startChar.setChar(symbol.charAt(0));
			    }
			    
			    Context.prevChar = yytext().charAt(0);
			    break;
				
			}
	"}" 	{
				if (Context.prevChar == '{') {
					context = context.exit(); yybegin(SEPA);
					++Context.counter;
					Context.prevChar = yytext().charAt(0);
				} else {
					throwDefaultException();
				}
			}
	"("		{
				if (Context.prevChar == '{' || Context.prevChar == ',') {
					yybegin(CHARA);
					Context.prevChar = yytext().charAt(0);
				} else {
					throwDefaultException();
				}
			}
	[{),]		{
				throwDefaultException();
			}
}

<TUPLE_VALUE> {Symbol} | ")" {
	String symbol = yytext();
	if (symbol.charAt(0) == ')') {
		Context.tuples.crtValue = "";
		Context.tuples.prevKey = null;
		yybegin(SEPA);
	} else {
		yybegin(TUPLE_VALUE);
		addToRValue(symbol.charAt(0));
	}
	Context.prevChar = yytext().charAt(0);
}

<SEPA> { //Read a separator and decide what is next
    ","				{
    					if (context == null) {
		            		if (Context.type == Context.Types.R) {
			            		context = new Context(Context.Types.S);
			            		context = context.enter(yystate(), Context.Types.S);
			            		yybegin(CHARA);
		            		} else {
		            			yybegin(SEPA);
		            		}
		            	} else {
		            		yybegin(CHARA);
		            	}
		            	Context.prevChar = yytext().charAt(0);
					}
	"}"				{
						if (Context.prevChar == ',' || Context.prevChar == '}' ||
							Context.prevChar == '(') {
							
							throwDefaultException();
						}
						
    					context = context.exit(); yybegin(SEPA);
    					++Context.counter;
    					Context.prevChar = yytext().charAt(0);
					}
	"{"				{
						if (Context.prevChar == '(' || Context.prevChar == ',') {
							Context.Types type = null;
							
							switch(Context.counter) {
								case 0:
									type = Context.Types.V;
									break;
								case 1:
									type = Context.Types.E;
									break;
								case 2:
									type = Context.Types.R;
									break;
								case 3:
									type = Context.Types.S;
									break;
								default:
									System.out.println("Unkown state: " + Context.counter);
									break;
							}
							
							context = new Context(type);
							context = context.enter(yystate(), type);
							yybegin(CHARA);
							Context.prevChar = yytext().charAt(0);
						} else {
							throwDefaultException();
						}
					}
	")"				{	
						context = context.exit();
						Context.prevChar = yytext().charAt(0);
					}
	[^{}),]		{
						throw new java.io.IOException("Wrong character");
					}
}