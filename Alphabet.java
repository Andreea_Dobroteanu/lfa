
import java.util.LinkedList;

public class Alphabet {
	LinkedList<Item> symbols;
	
	public Alphabet() {
		this.symbols = new LinkedList<Item>();
	}
	
	public void addSymbol(char c, boolean isUseless) {
		this.symbols.push(new Item(c, isUseless));
	}
	
	public boolean contains(char c) {
		for (Item s : symbols) {
			if (s.character == c) {
				return true;
			}
		}
		return false;
	}
	
	public void remove(Item e) {
		// TODO Auto-generated method stub
		for (int i = 0; i < symbols.size(); i++) {
			if (symbols.get(i).character == e.character) {
				symbols.remove(i);
			}
		}
	}
	
	public void removeAll(LinkedList<Item> list) {
		for (int i = 0; i < list.size(); i++) {
			remove(list.get(i));
		}
	}
	
	@Override
	public String toString() {
		String result = "{";
		String delim = "";
		for(Item c:this.symbols){
			result += delim + c.character;
			delim = ",";
		}
		result += "}";
		return result;
	}
}
