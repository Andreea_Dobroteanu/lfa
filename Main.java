import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Main {

	public static void main(String[] args) {
		BufferedReader br;
		
		if (args.length == 0) {
			System.out.println("Argument error");
		}
		
		try {
			br = new BufferedReader(new FileReader("grammar"));
			Flexer scanner = new Flexer(br);
			
			switch (args[0]) {
				case "--has-e":
					scanner.yylex();
					scanner.checkSynError();
					scanner.checkSemError();
					System.out.println();
					System.out.print(scanner.hasE());
					break;
				case "--is-void":
					scanner.yylex();
					scanner.checkSynError();
					scanner.checkSemError();
					System.out.println();
					System.out.print(scanner.isVoid());
					break;
				case "--useless-nonterminals":
					scanner.yylex();
					scanner.checkSynError();
					scanner.checkSemError();
					scanner.uselessChars();
					break;
				default:
					throw new IllegalArgumentException("Argument error");
			}			
		} catch (IllegalArgumentException e) {
			System.err.print(e.getMessage());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("Syntax error");
		}
	}

}
