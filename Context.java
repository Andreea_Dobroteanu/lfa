
import java.io.IOException;
import java.util.LinkedList;

public class Context {

	public static Alphabet allLetters;
	public static Alphabet containingLetters;
	public static AlphaPairs tuples;
	public static StartChar startChar;
	public static LinkedList<Character> uselessChars = new LinkedList<Character>();
    public int state;
    public static int counter = 0;
	public static LinkedList<Context> contexts = new LinkedList<Context>();
	public static Types type;
	public static char prevChar = '\0';

	public static enum Types {
		V(0),
		E(1),
		R(2),
		S(3);

		public final int priority;
		Types(int priority) {
			this.priority = priority;
		}
	}
	
	public Context(Types newType) {
		type = newType;
		
		switch(type) {
			case V:
				allLetters = new Alphabet();
				break;
			case E:
				containingLetters = new Alphabet();
				break;
			case R:
				tuples = new AlphaPairs();
				break;
			case S:
				startChar = new StartChar();
				break;
		default:
			break;
		}
	}
	
	public static boolean hasE() {
		return tuples.checkHasE();
	}
	
	public static boolean isVoid() {
		chooseUseless();
		return uselessChars.contains(startChar.character);
	}
	
	public static void checkSemError() throws IllegalArgumentException {
		//	Check start letter
		if (!allLetters.contains(startChar.character)) {
			throw new IllegalArgumentException("Semantic error");
		}
		
		//	E not in V
		for (Item e : containingLetters.symbols) {
			if (!allLetters.contains(e.character)) {
				throw new IllegalArgumentException("Semantic error");
			}
		}
		
		// Check if all non-uppers are in alphabet
		for (Item v : allLetters.symbols) {
			if (!Character.isUpperCase(v.character) && !containingLetters.contains(v.character)) {
				throw new IllegalArgumentException("Semantic error");
			}
		}
		
		// Check if keys belongs to V
		for (Item r : tuples.pairs.keySet()) {
			if (!allLetters.contains(r.character)) {
				throw new IllegalArgumentException("Semantic error");
			}
		}
		
		//	Check if values belongs to V
		for (Item i : tuples.pairs.keySet()) {
			for (LinkedList<Item> j : tuples.pairs.get(i).values()) {
				for (Item value : j) {
					if (!Context.allLetters.contains(value.character) && value.character != 'e') {
						throw new IllegalArgumentException("Semantic error");
					}					
				}
			}
		}
	}
	
	public static void checkSynError() throws java.io.IOException {
		//	Start character should be nonterminal
		if (!Character.isUpperCase(startChar.character)) {
			throw new IOException("Syntax error");
		}
		
		//	Empty V
		if (allLetters.symbols.isEmpty()) {
			throw new IOException("Syntax error");
		}
		
		//	Nonterminals in E
		for (Item e : containingLetters.symbols) {
			if (Character.isUpperCase(e.character)) {
				throw new IOException("Syntax error");
			}
		}
		
		//	Terminal as key
		for (Item r : tuples.pairs.keySet()) {
			if (!Character.isUpperCase(r.character)) {
				throw new IOException("Syntax error");
			}
		}
		
		// Check if e is in any alphabet
		if (allLetters.contains('e') || containingLetters.contains('e')) {
			throw new IOException("Syntax error");
		}
		
		// Check for extra e in values
		for (Item i : tuples.pairs.keySet()) {
			for (LinkedList<Item> j : tuples.pairs.get(i).values()) {
				for (Item value : j) {
					if (value.character == 'e' && j.size() > 1) {
						throw new IOException("Syntax error");
					}
				}
			}
		}
	}
	
	public static void chooseUseless() {
		for (Item v : allLetters.symbols) {
			for (Item e : containingLetters.symbols) {
				if (v.character == e.character) {
					v.isUseless = e.isUseless;
				}
			}
		}
		
		boolean keepUnmarking = true;
		while (keepUnmarking) {
			keepUnmarking = false;
			for (Item key : tuples.pairs.keySet()) {
				for (Integer vkey : tuples.pairs.get(key).keySet()) {
					
					if (key.isUseless == false) continue;
					boolean unmarkUseless = true;
					
					
					if (tuples.keyHasE(key, vkey)) {
						key.isUseless = false;
						tuples.unmarkUselessValues(key);
						keepUnmarking = true;
						break;
					}
					
					for (Item value : tuples.pairs.get(key).get(vkey)) {
						if (value.isUseless) {
							unmarkUseless = false;
							break;
						}
					}
				
					if (unmarkUseless) {
						key.isUseless = false;
						tuples.unmarkUselessValues(key);
						keepUnmarking = true;
					}
				}
			}
		}
		
		for (Item v : allLetters.symbols) {
			for (Item key : tuples.pairs.keySet()) {
				if (v.character == key.character) {
					v.isUseless = key.isUseless;
				}
			}
		}
		
		allLetters.symbols.removeAll(containingLetters.symbols);
		
		for (Item v : allLetters.symbols) {
			if (v.isUseless) {
				uselessChars.add(v.character);
			}
		}
	}
	
	public static void printUselessChars() {
		chooseUseless();
		
		if (uselessChars.size() == 0) return;
		
		System.out.println();
		for (int i = 0; i < uselessChars.size() - 1; i++) {
			System.out.println(uselessChars.get(i));
		}
		System.out.print(uselessChars.get(uselessChars.size() - 1));
	}

	public Context enter(int state, Types type) {
        this.state = state;
		Context.contexts.push(this);
		return new Context(type);
	}

	public Context exit() {
		return null;
	}
	
	@Override
	public String toString() {
		return "";
	}
}
