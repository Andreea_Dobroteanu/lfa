import java.util.LinkedList;
import java.util.HashMap;

public class AlphaPairs {
	HashMap<Item, HashMap<Integer, LinkedList<Item>>> pairs;
	public Item prevKey = null;
	public boolean readingString = false;
	public String crtValue = "";
	public int crtValueIndex = -1;

	public AlphaPairs() {
		this.pairs = new HashMap<Item, HashMap<Integer, LinkedList<Item>>>();
	}
	
	public void addKey(char key) {
		for (Item itKey : pairs.keySet()) {
			if (itKey.character == key) {
				int newValKey = pairs.get(itKey).keySet().size();
				pairs.get(itKey).put(newValKey, new LinkedList<Item>());
				
				this.prevKey = itKey;
				this.crtValueIndex = newValKey;
				return;
			}
		}
		
		Item newKey = new Item(key);
		HashMap<Integer, LinkedList<Item>> newValues = new HashMap<Integer, LinkedList<Item>>();
		newValues.put(0, new LinkedList<Item>());
		pairs.put(newKey, newValues);
		this.prevKey = newKey;
		this.crtValueIndex = 0;
	}
	
	public void addValue(Item value) {
		if (Character.isUpperCase(value.character)) {
			value.isUseless = true;
		}
		
		pairs.get(prevKey).get(crtValueIndex).add(value);
	}
	
	public void unmarkUselessValues(Item key) {
		for (HashMap<Integer, LinkedList<Item>> allValues : pairs.values()) {
			for (LinkedList<Item> values : allValues.values()) {
				for (Item value : values) {
					if (key.character == value.character) {
						value.isUseless = false;
					}
				}
			}
		}
	}
	
	public boolean keyHasE(Item key, int vkey) {
		for (Item rv : pairs.get(key).get(vkey)) {
			if (rv.character == 'e') {
				return true;
			}
		}
		return false;
	}
	
	public boolean checkHasE() {
		removeAllTerminals();
		
		Item startChar = null;
		for (Item key : pairs.keySet()) {
			if (Context.startChar.character == key.character) {
				startChar = key;
			}
			for (Integer vkey : pairs.get(key).keySet()) {
				for (Item it : pairs.get(key).get(vkey)) {
					it.isUseless = true;
				}
			}
		}
		
		if (startChar == null) {
			return false;
		}
		
		LinkedList<Item> usefullKeys = new LinkedList<Item>();
		for (Item key : pairs.keySet()) {
			for (Integer vkey : pairs.get(key).keySet()) {					
				boolean shouldBreak = false;
				for (Item it : pairs.get(key).get(vkey)) {
					if (it.character == 'e') {
						key.isUseless = false;
						unmarkUselessValues(key);
						usefullKeys.add(key);
						shouldBreak = true;
						break;
					}
				}
				if (shouldBreak) {
					break;
				}
			}
		}
		
		LinkedList<Item> knownKeys = new LinkedList<Item>();
		knownKeys.push(startChar);
		for (LinkedList<Item> list : pairs.get(startChar).values()) {
			for (Item it : list) {
				knownKeys.add(it);
			}
		}
		
		boolean canStart = false, ok = false, hasVoid = false;
		for (Item key : usefullKeys) {
			canStart = false; ok = false;
			
			for (Item it : knownKeys) {
				if (it.character == key.character || it.character == 'e') {
					canStart = true;
				}
			}
			
			if (!canStart) continue;
				
			if(pairs.get(key).values().size() == 1) {
				return true;
			}
			
			for (Integer vkey : pairs.get(key).keySet()) {
				for (Item it : pairs.get(key).get(vkey)) {
					if (!it.isUseless) {
						hasVoid = true;
						break;
					}
				}
				
				if (hasVoid) {
					ok = true;
				}
			}
			
			if (!ok) {
				for (LinkedList<Item> list : pairs.get(startChar).values()) {
					for (Item i : list) {
						if (i.character == key.character) {
							ok = true;
							break;
						}
					}
					if (ok) break;
				}				
			}
			
			if (!ok) {
				break;
			}
		}
		
		if (ok) return true;
		
		return false;
	}
	
	public void removeAllTerminals() {
		boolean ok = false;
		while (!ok) {
			ok = true;
			for (Item i : pairs.keySet()) {
				for (LinkedList<Item> j : pairs.get(i).values()) {
					for (int k = 0; k < j.size(); k++) {
						if (!Character.isUpperCase(j.get(k).character) && j.get(k).character != 'e') {
							j.remove(k);
							ok = false;
						}
					}
				}
			}
		}
	}
}
