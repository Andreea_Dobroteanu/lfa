
public class Item {
	char character;
	boolean isUseless;
	
	Item(char newChar) {
		character = newChar;
		this.isUseless = true;
	}
	
	Item(char newChar, boolean isUseless) {
		character = newChar;
		this.isUseless = isUseless;
	}
}
